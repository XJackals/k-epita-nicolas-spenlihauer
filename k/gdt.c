#include "gdt.h"
#include "stdio.h"

#define NB_ENTRY 3

ptr_gdt gdt_ptr;
entry_gdt gdt_entry[NB_ENTRY];

void print_entry(unsigned int i)
{
  printf("---Entry-%d---\n", i);
  printf("*Limit_Low = %p\n", gdt_entry[i].limit_low);
  printf("*Base_low = %p\n", gdt_entry[i].base_low);
  printf("*Base_middle = %p\n", gdt_entry[i].base_middle);
  printf("*Access = %p\n", gdt_entry[i].access);
  printf("*Granularity = %p\n", gdt_entry[i].granularity);
  printf("*Base_high = %p\n", gdt_entry[i].base_high);
  printf("\n-------------\n");
}

void print_gdt()
{
  printf("-------------\n\r");
  printf("--GDT-Print--\n\r");
  printf("-------------\n\r");
  printf("*Limit = %d\n\r", gdt_ptr.limit);
  printf("*Base = %p\n\r", gdt_ptr.base);

  for (unsigned int i = 0; i < NB_ENTRY; i++)
  {
    print_entry(i);
  }
  printf("-------------\n\n\r");
  printf("--End-GDT----\n\n\r");
  printf("-------------\n\n\r");
}

void init_entry(unsigned long base, unsigned long limit, unsigned char access,
                unsigned char gran, unsigned int i)
{
  gdt_entry[i].limit_low = (limit & 0xFFFF);
  gdt_entry[i].base_low = (base & 0xFFFF);
  gdt_entry[i].base_middle = (base >> 16) & 0xFF;
  gdt_entry[i].access = access;
  gdt_entry[i].granularity = ((limit >> 16) & 0x0F);
  gdt_entry[i].granularity |= (gran & 0xF0);
  gdt_entry[i].base_high = (base >> 24) & 0xFF;
}

void init_gdt()
{
  // Null descriptor
  init_entry(0x0, 0x0, 0x0, 0x0, 0);
  //Code Segment
  init_entry(0x0, 0xFFFFFFF, 0x9A, 0xCF, 1);
  //Data Segment
  init_entry(0x0, 0xFFFFFFF, 0x92, 0xCF, 2);

  gdt_ptr.limit = sizeof(gdt_entry) - 1;
  gdt_ptr.base = (u32)&gdt_entry;

  print_gdt();
  asm volatile("lgdt %0\n"
              :
              : "m"(gdt_ptr)
              : "memory");
  
  
  asm volatile("movw $0x10, %ax \n \
                movw %ax, %ds \n \
                movw %ax, %es \n \
                movw %ax, %fs \n \
                movw %ax, %gs \n \
                ljmp $0x08, $next \n \
                next: \n");
  printf("End of init_gdt\n");
}
