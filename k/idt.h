#ifndef DEF_IDT
# define DEF_IDT

#include "k/types.h"
#include "stdio.h"
#include "string.h"
#include "isr_init.h"
#include "io.h"

struct idt_entry
{
  unsigned short base_low;
  unsigned short sel;
  unsigned char always0;
  unsigned char flags;
  unsigned short base_high;

} __attribute__((packed));
typedef struct idt_entry entry_idt;


struct idt_ptr
{
  unsigned short limit;
  unsigned int base;
} __attribute__((packed));
typedef struct idt_ptr ptr_idt;

void init_idt();
void init_idt_entry(unsigned int i, unsigned long base, unsigned short sel, unsigned char flags);

#endif
