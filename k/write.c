#include "write.h"
#include "io.h"
#include "string.h"



int write(const char *buf, size_t count)
{
  if (count < strlen(buf))
  {
    return -1;
  }
  for (size_t i = 0; i < count; i++)
  {
    outb(0x3f8, buf[i]);
  }
  return count;
}
