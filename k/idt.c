#include "idt.h"

entry_idt idt_entry[256];
ptr_idt idt_ptr;

void init_pic(void);
void init_idt_entry(unsigned int i, unsigned long base, unsigned short sel, unsigned char flags)
{
  idt_entry[i].base_low = (base & 0xFFFF);
  idt_entry[i].sel = sel;
  idt_entry[i].always0 = 0;
  idt_entry[i].flags = flags;
  idt_entry[i].base_high = (base >> 16) & 0xFFFF;
}

void init_idt()
{
  puts("Begin of init idt\n");

  idt_ptr.limit = (sizeof(entry_idt) * 256) - 1;
  idt_ptr.base = (u32)&idt_entry;

  memset(&idt_ptr, 0, sizeof(idt_entry));

  __asm__ volatile("lidt %0\n"
                  :
                  :"m"(idt_ptr)
                  :"memory");

  puts("End of init idt\n");
  isr_install();
  puts("End of init idt\n\n");

  puts("Begin of init PICs \n\n");
  init_pic();
  puts("End of init PICs \n\n");
}

void init_pic(void)
{
    /* Initialization of ICW1 */
    outb(0x20, 0x11);
    outb(0xA0, 0x11);

    /* Initialization of ICW2 */
    outb(0x21, 0x20);    /* start vector = 32 */
    outb(0xA1, 0x70);    /* start vector = 96 */

    /* Initialization of ICW3 */
    outb(0x21, 0x04);
    outb(0xA1, 0x02);

    /* Initialization of ICW4 */
    outb(0x21, 0x01);
    outb(0xA1, 0x01);

    /* mask interrupts */
    outb(0x21, 0x0);
    outb(0xA1, 0x0);
}
