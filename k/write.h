#ifndef DEF_WRITE
# define DEF_WRITE

#include "k/types.h"

int write(const char *buf, size_t count);


#endif
