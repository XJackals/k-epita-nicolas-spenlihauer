#ifndef DEF_GDT
# define DEF_GDT

#include "k/types.h"

struct gdt_entry
{
  unsigned short limit_low;
  unsigned short base_low;
  unsigned char base_middle;
  unsigned char access;
  unsigned char granularity;
  unsigned char base_high;
} __attribute__((packed));
typedef struct gdt_entry entry_gdt;

struct gdt_ptr
{
  unsigned short limit;
  unsigned int base;
} __attribute__((packed));
typedef struct gdt_ptr ptr_gdt;

void init_gdt();

#endif
